#!/bin/bash
set -e

if [ ! -d "/usr/share/kibana/plugins/elastalert-kibana-plugin" ]
then
	/usr/share/kibana/bin/kibana-plugin install "$ELASTALERT_PLUGIN"
	echo "elastalert-kibana-plugin.serverHost: elastalert" >> /usr/share/kibana/config/kibana.yml
	echo "elastalert-kibana-plugin.serverPort: 3030" >> /usr/share/kibana/config/kibana.yml
fi
