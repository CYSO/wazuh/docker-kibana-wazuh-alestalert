FROM wazuh/wazuh-kibana:3.10.2_7.5.0

# Install elastalart
ARG ELASTALERT_PLUGIN="https://github.com/bitsensor/elastalert-kibana-plugin/releases/download/1.1.0/elastalert-kibana-plugin-1.1.0-7.5.0.zip"
ADD elastalert-install.sh elastalert-install.sh
RUN bash elastalert-install.sh

# Run optimize step
RUN bin/kibana --optimize
